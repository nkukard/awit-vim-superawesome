  1. Clone the repository into your ~/.vim directory

    cd
    
    git clone https://gitlab.devlabs.linuxassist.net/allworldit/awit-vim-superawesome.git .vim

  1. Install the ViM RC file...

    ln -s $HOME/.vim/vimrc $HOME/.vimrc
    
  1. You need to have vim.nox installed, this is a fully featured ViM for the commandline...

    apt-get install -y vim.nox

  1. Install fonts

    apt-get install -y fonts-powerline

  1. Fire up ViM and run these commands...

    :PlugClean
    
    :PlugInstall

  1. Reboot to make sure all is good and the right fonts symbols get loaded

  1. Set your terminal font to...

    Liberation Mono
