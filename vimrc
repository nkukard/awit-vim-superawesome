" Setup colors
set t_Co=256

" Tab to 4 chars
set tabstop=4
set shiftwidth=4

" Turn syntax highlighting on
syntax on
" Filetypes based on extension
filetype on

" Show edit window details in bar at bottom
set title
" Always show status line
set ls=2
set statusline=%<\ %n:%f\ %m%r%y%=%-35.(line:\ %l\ of\ %L,\ col:\ %c%V\ (%P)%)
" Show incomplete commands
set showcmd
" Show ruler
set ruler

" Set autocomplete menu colors
highlight Pmenu ctermfg=254 ctermbg=233
highlight PmenuSel cterm=bold ctermfg=233 ctermbg=254

" Set search background
highlight search cterm=bold ctermbg=222 ctermfg=160

" Set color of cursor column
highlight CursorColumn ctermbg=238

" Set color of end of line for programming
highlight ColorColumn ctermbg=238

" Highlight line we're on
highlight CursorLine cterm=none ctermbg=235
set cursorline

" Highlight messed up tab/spaces
highlight ExtraWhitespaces ctermbg=202

" Switch whitespace handling based on file type
function! WhitespaceHighlight()
	if &filetype == 'python'
		call matchadd('ExtraWhitespaces','^\t')
		set colorcolumn=79
	elseif &filetype != ''
		call matchadd('ExtraWhitespaces',' \t')
		call matchadd('ExtraWhitespaces','\t ')
		call matchadd('ExtraWhitespaces','^  ')
		set colorcolumn=132
	endif
	call matchadd('ExtraWhitespaces','\s\+$')
endfunction


" Autocmd
au BufNewFile,BufRead * call WhitespaceHighlight()
au BufNewFile,BufRead *.md set filetype=markdown | call WhitespaceHighlight()
au BufNewFile,BufRead *.html.ep set filetype=php | call WhitespaceHighlight()
au BufNewFile,BufRead *.py set filetype=python | set expandtab softtabstop=4 | call WhitespaceHighlight()

" Don't use backup files
set nobackup
set nowb

" Auto-indentation
set backspace=indent,eol,start
set cindent
set cinoptions=>s,e0,n0,f0,{0,}0,^0,L-1,:s,=s,ls,b0,gs,hs,N0,ps,t0,is,+2s,c1,C1,/0,(2s,us,Us,w0,W2s,k2s,m1,M-1,j0,J0,)20,*70,#1
set cinkeys=0{,0},0),:,!^F,o,O,e

" Closing braces
"inoremap {      {}<Left>
"inoremap {<CR>  {<CR>}<Esc>O
"inoremap {{     {
"inoremap {}     {}
" Closing brackets
"inoremap        (  ()<Left>
"inoremap <expr> )  strpart(getline('.'), col('.')-1, 1) == ")" ? "\<Right>" : ")"

" Try keep on same column when moving in a file
set nostartofline

" Highlight search on
set hlsearch
" Ignore search case
set ignorecase


" Show matching brackets when text indicator is over them
set showmatch


" Pull vim settings from end of file
set modeline
" Use 3 lines at bottom of file
set modelines=3

" Only support unix filetypes
set fileformats=unix

" All unknown chars should be in hex
set dy=uhex

" No annoying sound on errors
set noerrorbells
set novisualbell
set t_vb=
set tm=500



"
" vim-plug start
"
call plug#begin('~/.vim/plugged')

" https://github.com/Shougo/neocomplete.vim
Plug 'Shougo/neocomplete.vim'
" https://github.com/Shougo/neosnippet
Plug 'Shougo/neosnippet'

" https://github.com/airblade/vim-gitgutter
Plug 'airblade/vim-gitgutter'

" https://github.com/bling/vim-airline
Plug 'bling/vim-airline'

" https://github.com/tpope/vim-fugitive
Plug 'tpope/vim-fugitive'

" https://github.com/vim-syntastic/syntastic
Plug 'vim-syntastic/syntastic'


" vim-plug end
call plug#end()



"
" Enable neocomplete
"
let g:neocomplete#enable_at_startup = 1


"
" neosnippet configuration

" Enable snipMate compatibility feature, this loads all snippets/ from all
" plugins.
let g:neosnippet#enable_snipmate_compatibility = 1

" We also have another snippet directory here...
let g:neosnippet#snippets_directory='~/.vim/snippets'
let g:neosnippet#disable_runtime_snippets = { '_' : 1 }


"
" Configure neosnippets
"

" Plugin key-mappings.
imap <C-k>     <Plug>(neosnippet_expand_or_jump)
smap <C-k>     <Plug>(neosnippet_expand_or_jump)
xmap <C-k>     <Plug>(neosnippet_expand_target)

" SuperTab like snippets behavior.
imap <expr><TAB> neosnippet#expandable_or_jumpable() ?
\ "\<Plug>(neosnippet_expand_or_jump)"
\: pumvisible() ? "\<C-n>" : "\<TAB>"
smap <expr><TAB> neosnippet#expandable_or_jumpable() ?
\ "\<Plug>(neosnippet_expand_or_jump)"
\: "\<TAB>"

" For conceal markers.
if has('conceal')
	set conceallevel=2 concealcursor=niv
endif


"
" Configure vim-gitgutter
"
set signcolumn=yes


"
" Configure vim-airline
"

let g:airline#extensions#tabline#enabled = 1
let g:airline_powerline_fonts = 1

" Disable mouse in vim, so we can actually copy paste between applications
set mouse=
